#! /usr/bin/env python
################################################################################
#     File Name           :     getmap.py
#     Created By          :     totorikira
#     Creation Date       :     [2017-08-02 20:27]
#     Last Modified       :     [2017-08-03 21:36]
#     Description         :     record the change of bilibili map
################################################################################

from urllib import request
import json
import datetime
from PIL import Image
import time
import os
import shutil


def main():
    '''
        Main Function By TotoriKira
    '''

    color = {
        '0': (0, 0, 0),
        '1': (255, 255, 255),
        '2': (252, 222, 107),
        '3': (255, 246, 209),
        '4': (125, 149, 145),
        '5': (113, 190, 214),
        '6': (59, 229, 219),
        '7': (254, 211, 199),
        '8': (184, 63, 39),
        '9': (250, 172, 142),
        'A': (0, 70, 112),
        'B': (5, 113, 151),
        'C': (68, 201, 95),
        'D': (119, 84, 255),
        'E': (255, 0, 0),
        'F': (255, 152, 0),
        'G': (151, 253, 220),
        'H': (248, 203, 140),
        'I': (46, 143, 175),
    }
    row, col = 720, 1280

    #  try:
    #      # 新建存储目录
    #      shutil.rmtree('./GIF')
    #      os.mkdir('./GIF')
    #  except FileNotFoundError as e:
    #      print("No GIF Creating")
    #      os.mkdir('./GIF')
    #
    #  try:
    #      # 新建存储目录
    #      shutil.rmtree('./Time')
    #      os.mkdir('./Time')
    #  except FileNotFoundError as e:
    #      print("No Time Creating")
    #      os.mkdir('./Time')

    # 计数器
    cnt = 1
    filecnt = 1

    file = open("rem%d.txt" % filecnt, "w")

    while True:
        try:
            # 检测过大文件 屎一般的分p
            if cnt == 3000:
                file.close()
                cnt = 1
                filecnt += 1
                file = open("rem%d.txt" % filecnt, "w")

            web = request.urlopen(
                "http://api.live.bilibili.com/activity/v1/SummerDraw/bitmap")
            data = web.read().decode("utf-8")

            # 获得当前的时间数据
            tmp = datetime.datetime.now()
            Filending = tmp.strftime('%m_%d_%H%M%S')
            print("Time: %s | cnt: %d " %
                  (tmp.strftime("%m-%d %H:%M:%S "), cnt))

            file.write(tmp.strftime("%m-%d %H:%M:%S")+"\n")
            file.write(data+"\n")

            #  ret = json.loads(data)

            #  bitmap = ret['data']['bitmap']
            #  bmp = [color[bitmap[i * col + j]]
            #         for j in range(col) for i in range(row)]
            #
            #  outputIm = Image.new("RGB", (row, col))
            #  outputIm.putdata(bmp)
            #  Im = outputIm.transpose(Image.ROTATE_270).transpose(
            #      Image.FLIP_LEFT_RIGHT)
            #  Im.save('./Time/time%s.bmp' % Filending)
            #  Im.save('./GIF/time%d.bmp' % cnt)

            cnt += 1

        except KeyboardInterrupt as e:
            print("Good Bye")
            break

        except Exception as e:
            print(e)

        time.sleep(5)

    return


if __name__ == "__main__":
    main()
